# Messaging App [![LICENSE](https://img.shields.io/badge/license-GNU%20GPLv3-blue)](LICENSE)

A simple messaging UI template.

## Built With

* [Flutter](https://flutter.dev)

## Authors
* **Nihar Lekhade** - *Developer* - [ghostEleven](https://gitlab.com/ghostEleven)

## Credits
Inspired from the [design](https://dribbble.com/shots/7440118-Messages-Chat-Freebie-5) by [Kévin Mercier](https://dribbble.com/ui_mercier)

## License

This project is licensed under GNU GPLv3. See [LICENSE](LICENSE) for more details.

## Screenshots

![App gif](Screenshots/Messaging.gif)