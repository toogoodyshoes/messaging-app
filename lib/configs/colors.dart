import 'package:flutter/material.dart';

class AppColor {
  static const Color primaryColor = Color(0xFF6E00DD);
  static const Color secondaryColor = Color(0xFFA826C7);
  static const Color offWhite = Color(0xFFF3F4F9);
  static const Color whatAWhite = Color(0xFFF7F8FB);
  static const Color otherWhite = Color(0xFFEAECF2);
}