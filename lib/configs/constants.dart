import 'package:messaging_app/models/message_model.dart';
import 'package:messaging_app/models/user_model.dart';

User jake = User(
  name: 'Jake',
  displayPriority: null,
  id: 0,
  imageUrl: 'assets/images/6.jpg',
  isOnline: true,
);

User shaun = User(
  name: 'Shaun',
  displayPriority: null,
  id: 1,
  imageUrl: 'assets/images/81.jpg',
  isOnline: true,
);

User tyler = User(
  name: 'Tyler',
  displayPriority: null,
  id: 2,
  imageUrl: 'assets/images/10.jpg',
  isOnline: false,
);

User raheem = User(
  name: 'Raheem',
  displayPriority: null,
  id: 3,
  imageUrl: 'assets/images/65.jpg',
  isOnline: false,
);

User malcolm = User(
  name: 'Malcolm',
  displayPriority: null,
  id: 4,
  imageUrl: 'assets/images/15.jpg',
  isOnline: true,
);

User natasha = User(
  name: 'Natasha',
  displayPriority: null,
  id: 5,
  imageUrl: 'assets/images/47.jpg',
  isOnline: false,
);

User jessica = User(
  name: 'Jessica',
  displayPriority: null,
  id: 6,
  imageUrl: 'assets/images/51.jpg',
  isOnline: false,
);

User shiela = User(
  name: 'Shiela',
  displayPriority: null,
  id: 7,
  imageUrl: 'assets/images/13.jpg',
  isOnline: false,
);

User donna = User(
  name: 'Donna',
  displayPriority: null,
  id: 8,
  imageUrl: 'assets/images/69.jpg',
  isOnline: false,
);

User tania = User(
  name: 'Tania',
  displayPriority: null,
  id: 9,
  imageUrl: 'assets/images/7.jpg',
  isOnline: true,
);

List<Message> messages = [
  Message(
    sender: natasha,
    text: 'Are you available for coffee today?',
    time: '13:34',
    unread: true,
  ),
  Message(
    sender: malcolm,
    text: 'Call me if anything changes.',
    time: 'now',
    unread: true,
  ),
  Message(
    sender: shaun,
    text: 'Thats not possible',
    time: '18:04',
    unread: false,
  ),
  Message(
    sender: shiela,
    text: 'Should I though?',
    time: 'now',
    unread: true,
  ),
  Message(
    sender: tyler,
    text: 'That is confirmed.',
    time: '04:56',
    unread: false,
  ),
  Message(
    sender: tania,
    text: 'Good night',
    time: '23:46',
    unread: false,
  ),
  Message(
    sender: jessica,
    text: 'No on killed jessica',
    time: '13:34',
    unread: true,
  ),
  Message(
    sender: raheem,
    text: 'See you at the cafe.',
    time: '13:34',
    unread: false,
  ),
  Message(
    sender: donna,
    text: 'Seems like that.',
    time: '13:34',
    unread: true,
  ),
];

List<Message> chatMessages = [
  Message(
    sender: malcolm,
    text: 'Hey! Are we meeting today as planned?',
    time: '09:48',
    unread: false,
  ),
  Message(
    sender: jake,
    text: 'Yes. At Cafe Reunion, 5 PM',
    time: '10:13',
    unread: false,
  ),
  Message(
    sender: malcolm,
    text: 'Cool, see you there.\nWho else is coming?',
    time: '10:48',
    unread: false,
  ),
  Message(
    sender: jake,
    text: 'Natasha and Raheem.\nI am not sure about Tyler.',
    time: '11:57',
    unread: false,
  ),
  Message(
    sender: malcolm,
    text: 'Great. See you then.',
    time: '11:58',
    unread: false,
  ),
];