import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:messaging_app/components/chatWin_appbar.dart';
import 'package:messaging_app/components/message_bubble.dart';
import 'package:messaging_app/components/message_composer.dart';
import 'package:messaging_app/configs/constants.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.dark,
      ),
    );
    var _height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(_height * 0.2),
        child: ChatWinAppBar(),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Expanded(
            child: Container(
              height: _height * 0.8 - 80,
              child: ListView.builder(
                padding: EdgeInsets.symmetric(
                  vertical: 10.0,
                  horizontal: 20.0,
                ),
                itemCount: chatMessages.length,
                itemBuilder: (BuildContext context, int index) =>
                    MessageBubble(index: index),
              ),
            ),
          ),
          MessageComposer(),
        ],
      ),
    );
  }
}
