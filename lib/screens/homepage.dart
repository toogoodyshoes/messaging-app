import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:messaging_app/components/gradient_app_bar.dart';
import 'package:messaging_app/components/header.dart';
import 'package:messaging_app/configs/colors.dart';
import 'package:messaging_app/configs/constants.dart';
import 'package:messaging_app/configs/index_provider.dart';
import 'package:messaging_app/screens/chat_screen.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Widget> _botNavBarIcons = [
    Icon(
      Entypo.home,
      size: 30.0,
    ),
    Icon(
      MaterialCommunityIcons.map_marker,
      size: 30.0,
    ),
    Icon(
      Entypo.message,
      size: 30.0,
      color: AppColor.primaryColor,
    ),
    Icon(
      MaterialCommunityIcons.account,
      size: 30.0,
    ),
    Icon(
      MaterialCommunityIcons.dots_horizontal,
      size: 30.0,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.light,
      ),
    );
    var _height = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(_height * 0.2),
        child: GradientAppBar(),
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverPersistentHeader(
            delegate: MessageHeader(
                maxHeight: _height * 0.2, minHeight: _height * 0.2),
            floating: true,
            pinned: false,
          ),
          SliverList(
              delegate: SliverChildBuilderDelegate(
            (context, index) => Chat(index: index),
            childCount: messages.length,
          )),
        ],
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(horizontal: 15.0),
        height: 60.0,
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: _botNavBarIcons,
        ),
      ),
    );
  }
}

class Chat extends StatelessWidget {
  final int index;

  Chat({this.index});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Provider.of<CurrentChatIndex>(context, listen: false).ind = index;
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ChatScreen(),
          ),
        );
      },
      child: Container(
        height: 80.0,
        decoration: BoxDecoration(
          color: (messages[index].unread) ? AppColor.offWhite : Colors.white,
          border: Border(
            bottom: BorderSide(
              color: AppColor.offWhite,
              width: 1.0,
            ),
          ),
        ),
        child: Row(
          children: <Widget>[
            Container(
              height: 80.0,
              width: 5.0,
              decoration: BoxDecoration(
                gradient: (messages[index].unread)
                    ? LinearGradient(
                        colors: [
                          AppColor.primaryColor,
                          AppColor.secondaryColor
                        ],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                      )
                    : null,
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(left: 5.0, right: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        SizedBox(width: 10.0),
                        Stack(
                          fit: StackFit.passthrough,
                          alignment: Alignment.center,
                          children: <Widget>[
                            Container(
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              child: CircleAvatar(
                                radius: 30.0,
                                backgroundImage:
                                    AssetImage(messages[index].sender.imageUrl),
                              ),
                            ),
                            (messages[index].sender.isOnline)
                                ? Container(
                                    alignment: Alignment.topRight,
                                    height: 60,
                                    width: 60,
                                    decoration: BoxDecoration(
                                      color: Colors.transparent,
                                      borderRadius: BorderRadius.circular(30.0),
                                    ),
                                    child: CircleAvatar(
                                      radius: 6.5,
                                      backgroundColor: Colors.white,
                                      child: CircleAvatar(
                                        radius: 5.0,
                                        backgroundColor: Colors.green,
                                      ),
                                    ),
                                  )
                                : Container(),
                          ],
                        ),
                        SizedBox(width: 10.0),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              messages[index].sender.name,
                              textScaleFactor: 0.85,
                              style: TextStyle(
                                fontSize: 20.0,
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            Text(
                              messages[index].text,
                              textScaleFactor: 0.85,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w300,
                                fontSize: 15.0,
                                color: Colors.black45,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          messages[index].time,
                          textScaleFactor: 0.85,
                          style: TextStyle(
                            fontSize: 15.0,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Text(
                          '',
                          textScaleFactor: 0.85,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.w300,
                            fontSize: 15.0,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
