class User {
  final String name;
  final int displayPriority;
  final int id;
  final String imageUrl;
  final bool isOnline;

  User({this.name, this.displayPriority, this.id, this.imageUrl, this.isOnline});
}