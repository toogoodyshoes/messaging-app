import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:messaging_app/configs/colors.dart';

class GradientAppBar extends StatefulWidget {

  @override
  _GradientAppBarState createState() => _GradientAppBarState();
}

class _GradientAppBarState extends State<GradientAppBar> {
  @override
  Widget build(BuildContext context) {

    var _height = MediaQuery.of(context).size.height;

    return Material(
      color: Colors.transparent,
          child: Container(
        height: _height * 0.2,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [AppColor.primaryColor, AppColor.secondaryColor],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
        ),
        child: Padding(
          padding: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top,
              left: 20.0,
              right: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Feather.chevron_left,
                size: 25.0,
                color: Colors.white,
              ),
              Container(
                height: _height * 0.08,
                width: MediaQuery.of(context).size.width - 80,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30.0),
                  gradient: LinearGradient(
                    colors: [
                      AppColor.primaryColor,
                      AppColor.secondaryColor
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.3),
                      offset: Offset(0.0, 5.0),
                      blurRadius: 12.0,
                    )
                  ],
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Search...',
                        textScaleFactor: 0.85,
                        style: TextStyle(
                          color: Colors.white60,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w300,
                          fontSize: 18.0,
                        ),
                      ),
                      Icon(
                        Feather.search,
                        color: Colors.white,
                        size: 20.0,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
