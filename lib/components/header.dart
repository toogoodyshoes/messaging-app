import 'package:flutter/material.dart';

class Header extends StatefulWidget {
  @override
  _HeaderState createState() => _HeaderState();
}

class _HeaderState extends State<Header> {
  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    int newMessages = 2;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      color: Colors.white,
      height: _height * 0.2,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Messages',
            textScaleFactor: 0.85,
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Arial',
              fontWeight: FontWeight.bold,
              fontSize: 40.0,
            ),
          ),
          SizedBox(height: 10.0),
          Text(
            'You have $newMessages new messages',
            textScaleFactor: 0.85,
            style: TextStyle(
              color: Colors.black45,
              fontFamily: 'Arial',
              fontSize: 20.0,
            ),
          ),
        ],
      ),
    );
  }
}

class MessageHeader extends SliverPersistentHeaderDelegate {
  MessageHeader({this.maxHeight, this.minHeight});

  final double minHeight;
  final double maxHeight;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Header();
  }

  @override
  double get maxExtent => maxHeight;

  @override
  double get minExtent => minHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
