import 'package:flutter/material.dart';
import 'package:messaging_app/configs/colors.dart';
import 'package:messaging_app/configs/constants.dart';

class MessageBubble extends StatelessWidget {

  final int index;

  MessageBubble({this.index});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Padding(
          padding: chatMessages[index].sender.id == 0
              ? EdgeInsets.only(left: 50.0, top: 20.0)
              : EdgeInsets.only(right: 50.0, top: 20.0),
          child: ClipPath(
            clipper: chatMessages[index].sender.id == 0
                ? UserBubble()
                : SenderBubble(),
            child: Container(
              padding: EdgeInsets.fromLTRB(
                chatMessages[index].sender.id == 0 ? 15.0 : 25.0,
                15.0,
                15.0,
                15.0,
              ),
              decoration: chatMessages[index].sender.id == 0
                  ? BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          AppColor.primaryColor,
                          AppColor.secondaryColor
                        ],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                      ),
                    )
                  : BoxDecoration(
                      color: AppColor.otherWhite,
                    ),
              child: Text(
                chatMessages[index].text,
                textScaleFactor: 0.85,
                style: TextStyle(
                  fontFamily: 'Fira Sans',
                  fontSize: 18.0,
                  fontWeight: FontWeight.w300,
                  color: chatMessages[index].sender.id == 0
                      ? Colors.white
                      : Colors.black87,
                ),
              ),
            ),
          ),
        ),
        SizedBox(height: 5.0),
        Align(
          alignment: chatMessages[index].sender.id == 0 ? Alignment.centerRight : Alignment.centerLeft,
          child: Text(
            chatMessages[index].time,
            textScaleFactor: 0.85,
            style: TextStyle(
              fontFamily: 'Lucida Grande',
              fontSize: 12.0,
              color: Colors.grey,
            ),
          ),
        ),
      ],
    );
  }
}

class UserBubble extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.moveTo(size.width, size.height);
    path.lineTo(size.width - 10, size.height - 10);
    path.lineTo(size.width - 10, 10);
    path.quadraticBezierTo(size.width - 10, 0, size.width - 20, 0);
    path.lineTo(10, 0);
    path.quadraticBezierTo(0, 0, 0, 10);
    path.lineTo(0, size.height - 10);
    path.quadraticBezierTo(0, size.height, 10, size.height);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

class SenderBubble extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.moveTo(0, size.height);
    path.lineTo(10, size.height - 10);
    path.lineTo(10, 10);
    path.quadraticBezierTo(10, 0, 20, 0);
    path.lineTo(size.width - 10, 0);
    path.quadraticBezierTo(size.width, 0, size.width, 10);
    path.lineTo(size.width, size.height - 10);
    path.quadraticBezierTo(
        size.width, size.height, size.width - 10, size.height);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
