import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:messaging_app/configs/colors.dart';
import 'package:messaging_app/configs/constants.dart';
import 'package:messaging_app/configs/index_provider.dart';
import 'package:provider/provider.dart';

class ChatWinAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _index = Provider.of<CurrentChatIndex>(context).index;

    return Container(
      height: _height * 0.2,
      decoration: BoxDecoration(
        color: AppColor.whatAWhite,
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.2),
            offset: Offset(0.0, 0.0),
            blurRadius: 8.0,
          ),
        ],
      ),
      child: Padding(
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).padding.top,
          left: 20.0,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    SystemChrome.setSystemUIOverlayStyle(
                      SystemUiOverlayStyle(
                        statusBarIconBrightness: Brightness.light,
                      ),
                    );
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Feather.chevron_left,
                    size: 30.0,
                    color: Colors.black45,
                  ),
                ),
                SizedBox(width: 10.0),
                Stack(
                  fit: StackFit.passthrough,
                  alignment: Alignment.center,
                  children: <Widget>[
                    Container(
                      height: 60,
                      width: 60,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30.0),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.2),
                            offset: Offset(5.0, 5.0),
                            blurRadius: 5.0,
                          ),
                        ],
                      ),
                      child: CircleAvatar(
                        radius: 30.0,
                        backgroundImage:
                            AssetImage(messages[_index].sender.imageUrl),
                      ),
                    ),
                    (messages[_index].sender.isOnline)
                        ? Container(
                            alignment: Alignment.topRight,
                            height: 60,
                            width: 60,
                            decoration: BoxDecoration(
                              color: Colors.transparent,
                              borderRadius: BorderRadius.circular(30.0),
                            ),
                            child: CircleAvatar(
                              radius: 6.5,
                              backgroundColor: Colors.white,
                              child: CircleAvatar(
                                radius: 5.0,
                                backgroundColor: Colors.green,
                              ),
                            ),
                          )
                        : Container(),
                  ],
                ),
                SizedBox(width: 10.0),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      messages[_index].sender.name,
                      textScaleFactor: 0.85,
                      style: TextStyle(
                        fontSize: 20.0,
                        fontFamily: 'Arial',
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(
                      messages[_index].sender.isOnline
                          ? 'Online'
                          : '2 mins ago',
                          textScaleFactor: 0.85,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontFamily: 'Arial',
                        fontWeight: FontWeight.w300,
                        fontSize: 15.0,
                        color: Colors.black45,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.grey,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30.0),
                  bottomLeft: Radius.circular(30.0),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.fromLTRB(15.0, 15.0, 40.0, 15.0),
                child: Icon(
                  Feather.search,
                  size: 20.0,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
