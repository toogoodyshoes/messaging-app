import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:messaging_app/configs/colors.dart';

class MessageComposer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15.0),
      height: 80.0,
      decoration: BoxDecoration(
        color: AppColor.whatAWhite,
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.2),
            offset: Offset(0.0, 0.0),
            blurRadius: 8.0,
          ),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(
            AntDesign.leftsquare,
            color: Colors.grey,
            size: 30.0,
          ),
          SizedBox(
            width: 300.0,
            child: TextField(
              maxLines: null,
              cursorColor: AppColor.primaryColor,
              textAlign: TextAlign.left,
              textAlignVertical: TextAlignVertical.center,
              style: TextStyle(
                fontFamily: 'Fira Sans',
                fontSize: 18.0,
                color: AppColor.primaryColor,
              ),
              decoration: InputDecoration(
                prefixIcon: Icon(
                  FontAwesome5Solid.smile,
                  color: Colors.grey,
                  size: 30.0,
                ),
                hintText: 'Type something...',
                hintStyle: TextStyle(
                  fontFamily: 'Fira Sans',
                  fontSize: 18.0,
                ),
                border: InputBorder.none,
                suffixIcon: Icon(
                  FontAwesome.send,
                  color: AppColor.primaryColor,
                  size: 30.0,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
